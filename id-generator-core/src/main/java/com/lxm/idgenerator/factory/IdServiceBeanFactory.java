package com.lxm.idgenerator.factory;

import com.lxm.idgenerator.configuration.AutoConfiguration;
import com.lxm.idgenerator.enums.IdType;
import com.lxm.idgenerator.impl.IdServiceImpl;
import com.lxm.idgenerator.provider.DefaultWorkerIdProvider;
import com.lxm.idgenerator.provider.ZookeeperWorkerIdProvider;
import com.lxm.idgenerator.service.intf.IdService;
import com.lxm.idgenerator.zookeeper.ZookeeperHelperProxy;
import com.lxm.idgenerator.zookeeper.ZookeeperHandlerImpl;
import com.lxm.idgenerator.zookeeper.ZookeeperStateListener;

/**
 * IdService bean factory
 * @author luoxiaomin
 * @version 1.0.0
 * @date 2018/6/28
 * @time 11:17
 */
public class IdServiceBeanFactory {

    /**
     * 获取默认的IdService,idtype为毫秒级, workerId默认为0
     * @return
     */
    public static IdService getService() {
        return getService(0L, IdType.MILLISECOND);
    }

    /**
     * 通过指定的workerId获取IdService, 默认idtype为毫秒级
     * @param workerId
     * @return
     */
    public static IdService getService(long workerId) {
        return getService(workerId, IdType.MILLISECOND);
    }

    /**
     * 通过指定的workerId和Id类型获取IdService
     * @param workerId
     * @param idType
     * @return
     */
    public static IdService getService(long workerId, IdType idType) {
        DefaultWorkerIdProvider defaultWorkerIdProvider = new DefaultWorkerIdProvider();
        defaultWorkerIdProvider.setWorkerId(workerId);
        IdServiceImpl idServiceImpl = new IdServiceImpl(defaultWorkerIdProvider, idType);
        return idServiceImpl;
    }

    public static IdService getService(AutoConfiguration configuration) {
        ZookeeperHelperProxy proxy = new ZookeeperHelperProxy(ZookeeperHandlerImpl.getInstance().configure(configuration));
        ZookeeperWorkerIdProvider zookeeperWorkerIdProvider = new ZookeeperWorkerIdProvider(proxy);
        try {
            zookeeperWorkerIdProvider.register();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Id Service zookeeper 注册id失败：" + e.getMessage());
        }
        IdServiceImpl idServiceImpl = new IdServiceImpl(zookeeperWorkerIdProvider,
                configuration.getIdType() ? IdType.SECOND : IdType.MILLISECOND);
        ZookeeperStateListener listener = new ZookeeperStateListener(idServiceImpl);
        proxy.addListener(listener);
        return idServiceImpl;
    }

}
